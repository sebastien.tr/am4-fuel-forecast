# AM4-Fuel-Forecast

AM4-Fuel-Forecast is a Discord bot specifically designed for the Sky Wings Alliance in the Airline Manager 4 game. The bot provides fuel and CO2 price forecasts for the game and is tailored to work on the Stretford Coalition servers. It utilizes a database to store and retrieve relevant data for generating forecasts. The bot includes two main commands: `!fuel` and `!fuel best`.

## Features

### !fuel
The `!fuel` command retrieves the forecast for the next 12 hours of fuel and CO2 prices that will be in the game. The bot fetches data from the database, formats it into an aesthetically pleasing embed message with emojis, and sends it in the same channel where the command was invoked. The embed message also shows the user's local time.

### !fuel best
The `!fuel best` command provides the forecast filtered with only bests fuel & CO2 prices

### Data Synchronization
The Discord bot continuously monitors a designated channel for current Fuel Prices. If the prices in the channel differ from the values stored in the database, the bot automatically updates the database with the latest prices. This ensures that the forecasts provided by the bot remain accurate and up to date.

## License

The AM4-Fuel-Forecast bot is released under the [MIT License](LICENSE).
