import sqlite3
from datetime import datetime, timedelta
import config


def get_price(best: bool):
    # Connect to the database
    conn = sqlite3.connect('prices.db')
    c = conn.cursor()

    # Get the current date and time in UTC
    current_datetime = datetime.utcnow()

    # Format the date and time as a string
    if int(current_datetime.strftime("%M")) >= 30:
        time_string = f"{current_datetime.strftime('%H')}:30"
    else:
        time_string = f"{current_datetime.strftime('%H')}:00"

    # Initialize variables for tracking table and offset
    current_table = int(current_datetime.strftime('%d'))

    # Get the table name
    table_name = f"Day{current_table}"

    c.execute(f"SELECT * FROM {table_name} WHERE TimeUTC >= '{time_string}' ORDER BY TimeUTC ASC LIMIT {config.forecast_rows}")
    data = c.fetchall()

    entries = convert_entries(data, current_datetime)

    remaining = config.forecast_rows - len(data)

    # If the current day wasn't enough to fill the configured forecast rows, we continue with the next day
    if remaining > 0:
        table_name = f"Day{get_next_day(current_datetime)}"
        c.execute(f"SELECT * FROM {table_name} WHERE TimeUTC >= '00:00' ORDER BY TimeUTC ASC LIMIT {remaining}")
        next_day_data = c.fetchall()
        next_day_entries = convert_entries(next_day_data, increment_date(current_datetime))
        entries.extend(next_day_entries)

    # Close the connection
    conn.close()

    return format_data(entries, best)


def format_data(entries, best: bool):
    filtered = []

    for entry in entries:
        if best is False or (entry['fuel'] <= config.green_limit['fuel'] or entry['co2'] <= config.green_limit['co2']):
            entry['fuel'] = green_or_red(entry, 'fuel')
            entry['co2'] = green_or_red(entry, 'co2')
            filtered.append(entry)

    return filtered


def green_or_red(entry, key):
    return f"{config.green_emoji if entry[key] <= config.green_limit[key] else config.red_emoji} {entry[key]}"


def convert_entries(data, current_datetime):
    entries = []

    for row in data:
        row_list = list(row)
        hour = int(row_list[0][:2])
        minute = int(row_list[0][3:])
        row_datetime = current_datetime.replace(hour=hour, minute=minute, second=0, microsecond=0)
        entries.append({
            'datetime': row_datetime,
            'fuel': row_list[1],
            'co2': row_list[2]
        })

    return entries


def get_next_day(date):
    next_day = increment_date(date)
    return int(next_day.strftime('%d'))


def increment_date(date):
    return date + timedelta(days=1)
