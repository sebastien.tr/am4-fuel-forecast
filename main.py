import logging
import os
import sys

import discord
from discord import Embed
from discord.ext import commands

import database
import config

intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix=config.command_prefix, intents=intents)
logger = logging.getLogger(__name__)


def create_embed(data):
    embed = Embed(title=config.embed_title,
                  color=discord.Color.blurple())

    embed.set_author(name=config.embed_author)
    embed.set_footer(text=config.embed_footer)

    content = ""

    for entry in data:
        ts = int(entry['datetime'].timestamp()) + 3600 * 2
        content += f"🕑 <t:{ts}:t> ⛽️: {entry['fuel']} ♻️: {entry['co2']}\n"

    embed.add_field(name=f"", value=content, inline=False)

    return embed


@bot.event
async def on_ready():
    logger.info("Bot is connected and ready.")


@bot.command()
async def fuel(ctx, message: str = ""):
    if ctx.channel.id == 1237032297497100310:
        best = message.upper() == "BEST"
        data = database.get_price(best)
        # Create a new embed message
        embed = create_embed(data)
        await ctx.send(embed=embed)


def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.FileHandler("output.log"),
            logging.StreamHandler(sys.stdout)
        ]
    )

    logger.info('Started')
    # Run the Bot
    key = os.environ['DISCORDKEY']
    logger.info("Discord key : {}".format(key))
    bot.run(key)
    logger.info('Finished')


if __name__ == '__main__':
    main()
