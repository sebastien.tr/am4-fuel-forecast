forecast_rows = 12 * 2  # 12 hours with 2 entries per hour

green_limit = {
    'fuel': 900,
    'co2': 140
}

green_emoji = "🟢"
red_emoji = "🔴"

command_prefix = '!'

embed_title = "Fuel & CO2 price forecast for the next {} hours".format(int(forecast_rows / 2))
embed_author = "Stretford Coalition Fuel Bot"
embed_footer = "By Majestic Skyline, thanks to Ben Airways"
